<?php
#
# Copyright (c) 2000-2018 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#
chdir("..");
include_once("webtask.php");
include_once("geni_defs.php");
chdir("apt");
include_once("profile_defs.php");
include_once("instance_defs.php");

#
# Return info about a previous experiment.
#
function Do_HistoryRecord()
{
    global $this_user, $ajax_args;

    if (!isset($ajax_args["uuid"])) {
	SPITAJAX_ERROR(1, "Missing uuid");
	return;
    }
    $uuid = $ajax_args["uuid"];
    if (!IsValidUUID($uuid)) {
	SPITAJAX_ERROR(1, "Not a valid UUID: $uuid");
	return;
    }
    $record = InstanceHistory::Lookup($uuid);
    if (!$record) {
	SPITAJAX_ERROR(1, "No such record: $uuid");
	return;
    }
    if (! (ISADMIN() || ISFOREIGN_ADMIN() || $record->CanView($this_user))) {
	SPITAJAX_ERROR(1, "You do not have permission to view this page!");
	return;
    }
    $blob = $record->record;
    $blob["slivers"] = $record->slivers();

    #
    # Need to add a few things.
    #
    $blob["created"]   = DateStringGMT($blob["created"]);
    $blob["started"]   = DateStringGMT($blob["started"]);
    $blob["start_at"]  = DateStringGMT($blob["start_at"]);
    $blob["destroyed"] = DateStringGMT($blob["destroyed"]);
    $blob["profile_uuid"] = null;
    $blob["profile_name"] = null;

    if ($profile = Profile::Lookup($blob["profile_id"],
                                   $blob["profile_version"])) {
        $blob["profile_name"] = $profile->name() . ":" . $profile->version();
        $blob["profile_uuid"] = $profile->uuid();
    }
    # Need to munge the urls since these slices are history.
    foreach ($blob["slivers"] as &$sliver) {
        $url = $sliver["public_url"];
        if ($url && preg_match("/publicid=\w*/", $url)) {
            $url = "https://" . parse_url($url, PHP_URL_HOST) .
                 "/showslicelogs.php?slice_uuid=" . $record->slice_uuid();
            $sliver["public_url"] = $url;
        }
    }
    SPITAJAX_RESPONSE($blob);
}

# Local Variables:
# mode:php
# End:
?>
